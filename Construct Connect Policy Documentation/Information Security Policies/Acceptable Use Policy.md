title: Acceptable Use Policy  
author: vCISO  
published: 2020-09-03  
effective date: 2020-09-03  
policy level: Annual General  
location or team applicability: All locations, all teams  
approvers: Jeff Cryder - EVP, Julie Storm - CPO  


## Objective

The purpose of this policy is to ensure information security and data privacy by defining the acceptable use of work assets, technologies, and access to ConstructConnect information. 

## Scope

This policy applies to all employees when using ConstructConnect information and IT assets (e.g., personal computers, workstations, tablets, etc.). 

## Responsibilities

Corporate Information Security Office (CISO)
: is responsible and accountable for execution of all procedures and operations referenced within the Acceptable Use Policy.

People and Culture
: is responsible for ensuring that all employees acknowledge their awareness of information security responsibilities and manages the disciplinary process for security policy violations by any employees or contractors.

Employees
: are responsible for compliance to this policy and the requirements defined within.

## Review

This document must be reviewed for updates annually or whenever there is a material change in ConstructConnect's business practices that may reasonably implicate the security, confidentiality, integrity or availability of data and systems. The CISO owns this Policy, but any update or change must be approved by the Risk Committee.

# Audit and Monitoring

ConstructConnect reserves the right to audit networks and systems and monitor use of all ConstructConnect-owned and issued property to ensure compliance with this policy. This includes the use of BYOD systems that interface with, are running on, or storing company-owned resources. 

# Acceptable Use

## Policy Compliance

Users must familiarize themselves and comply with all ConstructConnect policies governing the use, access, management and security of technological resources. Users should refer to relevant security policies cited at the end of this document as well as the ConstructConnect Employment Handbook which goes into detail about computer usage.

## Ownership

Corporate information, internet access, data assets, access to data assets, and applications owned, licensed, or managed by ConstructConnect remain the sole property of ConstructConnect. ConstructConnect property must not be loaned to or used by anyone else without prior written approval of IT Management or the CISO. 

## Responsibility

Employees who are provided access to, or use of, ConstructConnect assets are responsible for the safety and care of those assets. Users must promptly report the damage, theft, loss or unauthorized access of any corporate information or physical assets. 

## Within Duties

Employees may access, use or share ConstructConnect information only to the extent authorized and necessary to fulfill their assigned job duties. Use of information assets outside of this prescribed use may be subject to disciplinary action. 

## Best Judgement

Employees are responsible for exercising their best judgment regarding the reasonableness of personal use. If there is any uncertainty, employees should consult their supervisor or manager. 

## Return of Property

All property of ConstructConnect must be returned to the employees immediate supervisor or authorized designee when it is no longer necessary for job performance. In the event of termination, all property of ConstructConnect must be returned on the last day of work. 

## Data Transmission

Data that is owned by or contains sensitive information concerning ConstructConnect must only be transmitted in full compliance with the Data Management Policy, this includes but is not limited to measures like encryption or secure messaging/email. 

# Unacceptable Use

## No Unlicensed Material

Users must not use property of ConstructConnect to install, use, copy or distribute unlicensed or third-party copyrighted material (this includes torrenting content).

## No Harassment

Users must not use property of ConstructConnect to harass others through e-mail, direct message, message board, social media, or other dissemination avenues. 

## No Unauthorized Access

Users must not engage in unauthorized access to devices, networks or systems of ConstructConnect, vendors, employees or customers.

## No Offensive Material

Users must not use property of ConstructConnect to display, upload, download, distribute or participate in bulletin boards, social media or other electronic forum discussions regarding subject matter containing inappropriate materials or information that may be offensive to others. 

## No Media and Streaming

Users must not use property of ConstructConnect to download or stream movies, video clips, audio feeds, games or any activity using significant bandwidth of ConstructConnect internet access. Similarly, personal content such as media should not be stored on Company-owned IT storage mediums such as network shares, cloud service storage drives, or external hard drives. 

## No Dangerous Web Browsing

Users must not in any way endanger the integrity or security of ConstructConnect property by visiting untrusted websites. ConstructConnect may monitor internet use or restrict access to certain subject matter or websites.

## No Disabling Controls

Users must not uninstall, disable, or circumvent virus protection or security controls. If there is a business need for configuration changes, a request must be submitted using the approved Exception Management Process. 

## No Spam

Users must not use property of ConstructConnect to send unsolicited e-mail messages including junk mail, chain letters, Ponzi schemes, pyramid or multi-level marketing invitations, solicitations or advertising materials. 

## No Illegal Behavior

Users must not engage in any activity that is illegal under applicable local, state, federal or international law while using property of ConstructConnect. 

# Enforcement

Violations of this Acceptable Use Agreement will result in disciplinary action, in accordance with ConstructConnect's information security policies and procedures and human resources policies. 

Please see [REFERENCE TO PEOPLE AND CULTURE POLICIES] for details regarding ConstructConnect's disciplinary process. 

# REFERENCES

## Regulations

* PCI-DSS
* US Data Privacy Laws

## NIST CSF

* Protect

## Roper CIS Controls

* Cybersecurity Governance and Risk Management
* Data Protection 
* Email, Malware, and Web Protection
* Security Awareness

## Related Policies, Plans, Procedures and Guidelines

* Written Information Security Program
* Policies
    * Data Management Policy
    * Personnel Security Policy

