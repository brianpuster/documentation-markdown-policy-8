title: IT, Infastructure and Back Office - Flexible Schedule Policy
published: 2019-09-11
author: Bob Ven - CTO

**IT/INFRASTRUCTURE/BACK OFFICE FLEXIBLE WORK POLICY**

The company is committed to a work environment that accommodates our
People First/Family First values. Each team offers a
flexible/telecommuting schedule based on business sustainability and
individual performance. The following basic requirements must be met for
all teams:

-   Team members must have satisfactory attendance and cannot be on a
    Performance Improvement Plan.

-   The work week for all full-time regular team members is 40 hour per
    week and 8 hours per day

-   Flexible/telecommuting work schedules for non-exempt team members
    should include an unpaid meal period of 30 minutes at minimum.

-   There will be times when team members are required to depart from
    the flexible/telecommuting work schedule to accommodate changing
    situations and staffing needs. Reasonable notice will be provided
    when possible.

-   The day before a holiday falling on a work day is treated as a
    normal 8 hour business day.

**Early Release Friday**

-   On Fridays only, team members may leave 1.5 hours earlier than their
    normal schedule as long as individual performance goals are being
    met (i.e. 100% of month-to-date and year-to-date quota attainment)
    and 6.5 hours are worked for the day.

-   Team members on a Performance Improvement Plan are excluded from
    this benefit.

-   Managers reserve the right to ask team members to stay for their
    entire schedule due to business needs.

-   Early release and PTO cannot be combined. If PTO is taken on a
    Friday the entire day must be coded as PTO (8 hours). If a partial
    day is taken, PTO and hours worked must equal 8 hours. Lunch can be
    taken, but 6.5 hours should be worked that day and if salaried
    non-exempt, recorded as such in UltiPro.

**Schedule Changes/Telecommuting**

-   The standard hours for ConstructConnect are 8:00 am - 5:00 pm. The
    team allows formal flexible schedules outside of these hours at the
    discretion of the manager for special circumstances as long as 40
    productive hours are worked in the work week. Please see your
    manager to establish a flexible schedule.

-   Our team offers the opportunity to work from home one day per week
    on a pre-arranged schedule. A telecommuting agreement must be
    completed.

-   Additional time may be given at the manager's discretion.

-   Service Delivery positions are exempt from telecommuting.

-   Working from home is based on manager' discretion and the individual
    job description.

-   Up to two hours may be made up in a week without using PTO.

Team member acknowledges that they have read and understand the terms of
this agreement.
