title: BPM - MAJOR MARKET PERFORMANCE IMPROVEMENT PLAN
author: Howard Atkins - EVP & GM BPM
published: 2019-05-01

Sales Effectiveness Consultants’ performance will be reviewed by Sales Management on a monthly basis at the beginning of each month for the previous month’s performance against the individual’s established minimum monthly threshold.  If a Sales Effectiveness Consultant does not meet their full threshold, the performance improvement plan process will begin as outlined below.  Sales Effectiveness Consultants who have not yet completed the 90-day orientation period will not be subject to this process, but will be held responsible for a successful orientation period, which consists of:

#Orientation Period Performance Guidelines
- Satisfactory attendance – outlined in the Employment Handbook
- Attitude that is open to coaching and feedback
- Achieve a minimum of 50% of the ramp (orientation) period goal each month
- Completion of all required certifications

Upon successful completion of the orientation period, team members will enter the “regular” employment classification.  If team members are unable to demonstrate their ability to achieve a satisfactory level of performance during their orientation period, their employment could be terminated.

#Performance Improvement Process: 
- Achieved below 60% of quota for two consecutive months:  1st Stage Improvement Plan.
- While on 1st Stage PIP, any miss below 65%, team member will be escalated to 2nd Stage Improvement Plan OR a missed goal below 50%  can lead to termination.  This also applies to Stage 2 PIP.
- Stages run consecutively.
- In order to be removed from performance improvement plan you must maintain 65% of goal for rolling three-month period.

Management reserves the right to by-pass stages of the improvement process depending on the nature of the issue.

Minimum monthly threshhold as it pertains to the performance plan will be pro-rated for approved FMLA or bereavement absences.

This is a guideline for a performance improvement plan and is subject to change at the discretion of the Executive VP/GM, Contractors or CPO of ConstructConnect. 

I have read and acknowledge the Sales Effectiveness Consultant Performance Improvement Plan Process to be the process I will be held accountable to regarding my performance.
